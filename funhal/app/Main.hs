module Main where

import Lib
import System.Environment
import System.IO
import System.Environment
import System.Exit
import Data.List

data HalFunction = HalFunction {
    fname::[Char],
    fargs::[[Char]],
    code::[Char]
} deriving (Eq, Show)

data HalLambda = HalLambda {
    lfargs::[[Char]],
    lcode::[Char]
} deriving (Eq, Show)

data Operator = Operator {
    name::[Char],
    arguments::[Operator],
    dtype::[Char],
    list::[Operator],
    bool::Bool,
    char::Char,
    int::Integer,
    float::Float,
    quote::[Char],
    error::Integer,
    function::HalFunction,
    lambda::HalLambda
 } deriving (Eq, Show)

errorString 1 = "** car :: Failure :: Not a List **"
errorString 2 = "** cdr :: Failure :: Not a List **"
errorString 3 = "** cdr :: Failure :: Empty List **"
errorString 4 = "** eq? :: Failure **"
errorString 5 = "** cons :: Failure **"
errorString 11 = "** + :: Failure **"
errorString 12 = "** * :: Failure **"
errorString 13 = "** div :: Failure **"
errorString 14 = "** - :: Failure **"
errorString 15 = "** mod :: Failure **"
errorString 18 = "** cond :: Failure **"
errorString 84 = "** hal :: Failure **"
errorString 21 = "** define :: Failure **"
errorString 22 = "** > :: Failure **"
errorString 23 = "** < :: Failure **"
errorString 24 = "** >= :: Failure **"
errorString 25 = "** <= :: Failure **"
errorString index = "** hal :: Failure **"

errorIndex index = (Operator "error" [] "error" [] False '\0' 0 0 "" index (HalFunction "" [] "") (HalLambda [] ""))

readBool (Operator "var" a "bool" l b c i f q e hf hl) = b
readBool o = False

refactorQuote k [] = ""
refactorQuote '(' ('\'':xs) = "'" ++ refactorQuote '\'' xs
refactorQuote '(' ('q':'u':'o':'t':'e':xs) = "'" ++ refactorQuote '\'' xs
refactorQuote '(' (' ':xs) = refactorQuote '(' xs
refactorQuote '(' ('\t':xs) = refactorQuote '(' xs
refactorQuote prev ('\'':xs) = "('" ++ (getNextArg xs) ++ ")" ++ (refactorQuote ')' (recraftLine (skipOperator xs) (getNextArg xs)))
refactorQuote prev ('q':'u':'o':'t':'e':xs) = "('" ++ (getNextArg xs) ++ ")" ++ (refactorQuote ')' (recraftLine (skipOperator xs) (getNextArg xs)))
refactorQuote prev (x:xs) = [x] ++ (refactorQuote x xs)

extract_par [] 0 init = []
extract_par [] x init = []
extract_par (x:xs) step init = if (step == 0 && init == 1) then ([]) else(if x == '(' then ([x] ++ (extract_par xs (step + 1) 1)) else
                                        (if (x == ')') then ([x] ++ (extract_par xs (step - 1) 1)) else 
                                           ([x] ++ (extract_par xs step init))))
getThisArg [] active = []
getThisArg ('#':'t':xs) a = "#t" 
getThisArg ('#':'f':xs) a = "#f" 
getThisArg (x:xs) active = if ((x == ' ' || x == '\t') && active == 1) then [] else
                (if (x == '(') then extract_par ([x] ++ xs) 0 0 else 
                (if (x >= '0' && x <= '9') then [x] ++ (getThisArg xs 1) else (if (active == 1) then getThisArg xs 1 else getThisArg xs 0)))

getNextArg [] = []
getNextArg (')':xs) = []
getNextArg (' ':xs) = getNextArg xs
getNextArg ('\t':xs) = getNextArg xs
getNextArg ('\n':xs) = getNextArg xs
getNextArg (x:xs) = getThisArg ([x] ++ xs) 0

getOperator [] = []
getOperator (x:xs) = if (x /= ' ' && x /= '\t' && x /= '(') then [x] ++ (getOperator xs)  else []

skipOperatorMain line = (splitAt (length (getOperator (line))) line)
skipOperatorMain_ (x, y) = y
skipOperator line = skipSpaceTab (skipOperatorMain_ (skipOperatorMain line))
skipSpaceTab [] = []
skipSpaceTab (x:xs) = if (x /= ' ' && x /= '\t') then [x] ++ xs else skipSpaceTab xs

recraftLineMain (x, y) = y
recraftLine line lastArgs = skipSpaceTab (recraftLineMain (splitAt ((length lastArgs)) line))

getArguments line = if (getNextArg line /= "") then [getNextArg line] ++ getArguments (recraftLine (skipSpaceTab line) (getNextArg (skipSpaceTab line))) else []

escape_par [] = []
escape_par (x:xs) = if (x == '(') then xs else ([x] ++ xs)
skipPar ('(':xs) = xs
skipPar x = x

fct [] = errorIndex 84

createOperand "#t" = (Operator "var" [] "bool" [] True '\0' 0 0 "" 0 (HalFunction "" [] "") (HalLambda [] ""))
createOperand "#f" = (Operator "var" [] "bool" [] False '\0' 0 0 "" 0 (HalFunction "" [] "") (HalLambda [] ""))
createOperand x = (Operator "var" [] "int" [] False '\0' (read x :: Integer) 0 "" 0 (HalFunction "" [] "") (HalLambda [] ""))

isOperand [] = True
isOperand ('0':xs) = isOperand xs
isOperand ('1':xs) = isOperand xs
isOperand ('2':xs) = isOperand xs
isOperand ('3':xs) = isOperand xs
isOperand ('4':xs) = isOperand xs
isOperand ('5':xs) = isOperand xs
isOperand ('6':xs) = isOperand xs
isOperand ('7':xs) = isOperand xs
isOperand ('8':xs) = isOperand xs
isOperand ('9':xs) = isOperand xs
isOperand "#t" = True
isOperand "#f" = True
isOperand x = False

isOperator "'" = True
isOperator "quote" = True
isOperator "cons" = True
isOperator "car" = True
isOperator "cdr" = True
isOperator "cond" = True
isOperator "eq?" = True
isOperator "list" = True
isOperator "define" = True
isOperator "+" = True
isOperator "-" = True
isOperator "*" = True
isOperator "div" = True
isOperator "mod" = True
isOperator "<" = True
isOperator "<=" = True
isOperator ">" = True
isOperator ">=" = True
isOperator "lambda" = True
isOperator x = False

checkFarg [] [] = True
checkFarg (' ':xs) [] = True
checkFarg ('\t':xs) [] = True
checkFarg (')':xs) [] = True
checkFarg (x:xs) [] = False
checkFarg [] (y:ys) = False
checkFarg (x:xs) (y:ys) = if (x == y) then checkFarg xs ys else False

checkFargs [] code = False
checkFargs (x:xs) code = checkFarg code x || checkFargs xs code

replaceFargsSplit farg arg (a,b) = arg ++ b
replaceFargs farg arg code = replaceFargsSplit farg arg (splitAt (length farg) code)

replaceFargs2 [] a code = code
replaceFargs2 f [] code = code
replaceFargs2 (f:fs) (a:as) code = if (checkFarg code f) then replaceFargs f a code else replaceFargs2 fs as code

newfref fargs args prev [] = []
newfref fargs args ')' (c:cs) = if (checkFargs fargs ([c] ++ cs)) then (newfref fargs args c (replaceFargs2 fargs args ([c] ++ cs))) else [c] ++ newfref fargs args c cs
newfref fargs args '(' (c:cs) = if (checkFargs fargs ([c] ++ cs)) then (newfref fargs args c (replaceFargs2 fargs args ([c] ++ cs))) else [c] ++ newfref fargs args c cs
newfref fargs args ' ' (c:cs) = if (checkFargs fargs ([c] ++ cs)) then (newfref fargs args c (replaceFargs2 fargs args ([c] ++ cs))) else [c] ++ newfref fargs args c cs
newfref fargs args '\t' (c:cs) = if (checkFargs fargs ([c] ++ cs)) then (newfref fargs args c (replaceFargs2 fargs args ([c] ++ cs))) else [c] ++ newfref fargs args c cs
newfref fargs args '\n' (c:cs) = if (checkFargs fargs ([c] ++ cs)) then (newfref fargs args c (replaceFargs2 fargs args ([c] ++ cs))) else [c] ++ newfref fargs args c cs
newfref fargs args prev (c:cs) = [c] ++ (newfref fargs args c cs) 

fullref fargs args code = newfref fargs args ' ' code

call fn [] args fns = errorIndex 21 
call fn ((HalFunction fname fargs function):fs) args fns = if (fn == fname) then executeSkip (fullref fargs args function) fns else (call fn fs args fns) 

letGetName line = getOperator (skipPar line)

letGetValueMain [x] fns = executeSkip x fns
letGetValue line fns = letGetValueMain (getArguments (skipOperator (skipPar line))) fns

letCraftFargs [] = []
letCraftFargs (x:xs) = [letGetName x] ++ letCraftFargs xs

letCraftArgs [] fns = []
letCraftArgs (x:xs) fns = [printing (letGetValue x fns)] ++ letCraftArgs xs fns

letRun fargs args code fns = executeSkip (fullref fargs args code) fns
letMotor [line, code] fns = letRun (letCraftFargs (getArguments (skipPar line))) (letCraftArgs (getArguments (skipPar line)) fns) code fns

carMain (x:xs) = x
car [(Operator "error" a "error" l b c i f q e hf hl)] = errorIndex e
car [(Operator n a t l b c i f q e hf hl)] = if (n =="var" && t == "list") then (head l) else (errorIndex 1)
car k = errorIndex 1 

cdrMain [] = errorIndex 3 
cdrMain (x:xs) = (Operator "var" [] "list" xs False '\0' 0 0 "" 0 (HalFunction "" [] "") (HalLambda [] ""))
cdr [(Operator "error" a "error" l b c i f q e hf hl)] = errorIndex e
cdr [(Operator n a t l b c i f q e hf hl)] = if (n =="var" && t == "list") then (cdrMain l) else (errorIndex 2)
cdr k = errorIndex 2

cons [(Operator "error" a "error" l b c i f q e hf hl), y] = errorIndex e
cons [x, (Operator "error" a "error" l b c i f q e hf hl)] = errorIndex e
cons [x, (Operator "var" a "int" l b c i f q e hf hl)] = (Operator "var" a "list" ([x] ++ [createOperand (show i)]) b c i f q e hf hl)
cons [x, (Operator "var" a "list" l b c i f q e hf hl)] = (Operator "var" a "list" ([x] ++ l) b c i f q e hf hl)
cons [x,y] = errorIndex 5
cons k = errorIndex 5

equalList [] [] = True
equalList [] (y:ys) = False
equalList (x:xs) []  = False
equalList (x:xs) (y:ys) = readBool (equalInterior x y) && equalList xs ys

equalInterior x (Operator "error" a "error" l b c i f q e hf hl) = errorIndex e
equalInterior (Operator "error" a "error" l b c i f q e hf hl) x = errorIndex e
equalInterior (Operator "var" a "list" l b c i f q e hf hl) (Operator "var" a2 "list" l2 b2 c2 i2 f2 q2 e2 hf2 hl2) = (Operator "var" [] "bool" [] (equalList l l2) '\0' 0 0 "" 0 (HalFunction "" [] "") (HalLambda [] ""))
equalInterior (Operator "var" a "bool" l b c i f q e hf hl) (Operator "var" a2 "bool" l2 b2 c2 i2 f2 q2 e2 hf2 hl2) = (Operator "var" [] "bool" [] (b == b2) '\0' 0 0 "" 0 (HalFunction "" [] "") (HalLambda [] ""))
equalInterior (Operator "var" a "char" l b c i f q e hf hl) (Operator "var" a2 "char" l2 b2 c2 i2 f2 q2 e2 hf2 hl2) = (Operator "var" [] "bool" [] (c == c2) '\0' 0 0 "" 0 (HalFunction "" [] "") (HalLambda [] ""))
equalInterior (Operator "var" a "int" l b c i f q e hf hl) (Operator "var" a2 "int" l2 b2 c2 i2 f2 q2 e2 hf2 hl2) = (Operator "var" [] "bool" [] (i == i2) '\0' 0 0 "" 0 (HalFunction "" [] "") (HalLambda [] ""))
equalInterior (Operator "var" a "float" l b c i f q e hf hl) (Operator "var" a2 "float" l2 b2 c2 i2 f2 q2 e2 hf2 hl2) = (Operator "var" [] "bool" [] (f == f2) '\0' 0 0 "" 0 (HalFunction "" [] "") (HalLambda [] ""))
equalInterior o o2 = errorIndex 4
equal [o, o2] = equalInterior o o2

list_fn [] = (Operator "var" [] "list" [] False '\0' 0 0 "" 0 (HalFunction "" [] "") (HalLambda [] ""))
list_fn (x:xs) = (Operator "var" [] "list" ([x] ++ xs) False '\0' 0 0 "" 0 (HalFunction "" [] "") (HalLambda [] ""))

plus [x, (Operator "error" a "error" [] _ '\0' i f "" e hf hl)] = errorIndex e
plus [(Operator "error" a "error" [] _ '\0' i f "" e hf hl), y] = errorIndex e
plus [(Operator "var" a "int" [] _ '\0' i 0 "" 0 hf hl), (Operator "var" a2 "int" [] b '\0' i2 0 "" 0 hf2 hl2)] = (Operator "var" a "int" [] b '\0' (i2 + i) 0 "" 0 (HalFunction "" [] "") (HalLambda [] ""))
plus k = errorIndex 11

mul [x, (Operator "error" a "error" [] _ '\0' i 0 "" e hf hl)] = errorIndex e
mul [(Operator "error" a "error" [] _ '\0' i 0 "" e hf hl), y] = errorIndex e
mul [(Operator "var" a "int" [] _ '\0' i 0 "" e hf hl), (Operator "var" a2 "int" [] b '\0' i2 0 "" 0  hf2 hl2)] = (Operator "var" a "int" [] b '\0' (i * i2) 0 "" 0 (HalFunction "" [] "") (HalLambda [] ""))
mul k = errorIndex 12

sub [(Operator "var" a "int" [] _ '\0' i 0 "" 0 hf hl), (Operator "var" a2 "int" [] b '\0' i2 0 "" 0 hf2 hl2)] = (Operator "var" a "int" [] b '\0' (i - i2) 0 "" 0 (HalFunction "" [] "") (HalLambda [] ""))
sub [(Operator "error" a "error" [] _ '\0' i 0 "" e hf hl), y] = errorIndex e
sub [x, (Operator "error" a "error" [] _ '\0' i 0 "" e hf hl)] = errorIndex e
sub k = errorIndex 14


division [(Operator "var" a "int" [] _ '\0' i 0 "" 0 hf hl), (Operator "var" a2 "int" [] b '\0' 0 0 "" 0 hf2 hl2)] = errorIndex 13
division [(Operator "var" a "int" [] _ '\0' i 0 "" 0 hf hl), (Operator "var" a2 "int" [] b '\0' i2 0 "" 0 hf2 hl2)] = (Operator "var" a "int" [] b '\0' (div i i2) 0 "" 0 (HalFunction "" [] "") (HalLambda [] ""))
division [(Operator "error" a "error" [] _ '\0' i 0 "" e hf hl), y] = errorIndex e
division [x, (Operator "error" a "error" [] _ '\0' i 0 "" e hf hl)] = errorIndex e
division k = errorIndex 13

modulo [(Operator "var" a "int" [] _ '\0' i 0 "" 0 hf hl), (Operator "var" a2 "int" [] b '\0' i2 0 "" 0 hf2 hl2)] = (Operator "var" a "int" [] b '\0' (mod i i2) 0 "" 0 (HalFunction "" [] "") (HalLambda [] ""))
modulo [x, (Operator "var" a2 "int" [] b '\0' 0 0 "" 0 hf2 hl2)] = errorIndex 15
modulo [(Operator "error" a "error" [] _ '\0' i 0 "" e hf hl), y] = errorIndex e
modulo [x, (Operator "error" a "error" [] _ '\0' i 0 "" e hf hl)] = errorIndex e
modulo k = errorIndex 15

sup [(Operator "var" a "int" [] _ '\0' i 0 "" 0 hf hl), (Operator "var" a2 "int" [] b '\0' i2 0 "" 0 hf2 hl2)] = (Operator "var" a "bool" [] (i > i2) '\0' 0 0 "" 0 (HalFunction "" [] "") (HalLambda [] ""))
sup [(Operator "error" a "error" [] _ '\0' i 0 "" e hf hl), y] = errorIndex e
sup [x, (Operator "error" a "error" [] _ '\0' i 0 "" e hf hl)] = errorIndex e
sup k = errorIndex 22

inf [(Operator "var" a "int" [] _ '\0' i 0 "" 0 hf hl), (Operator "var" a2 "int" [] b '\0' i2 0 "" 0 hf2 hl2)] = (Operator "var" a "bool" [] (i < i2) '\0' 0 0 "" 0 (HalFunction "" [] "") (HalLambda [] ""))
inf [(Operator "error" a "error" [] _ '\0' i 0 "" e hf hl), y] = errorIndex e
inf [x, (Operator "error" a "error" [] _ '\0' i 0 "" e hf hl)] = errorIndex e
inf k = errorIndex 23

supEqual [(Operator "var" a "int" [] _ '\0' i 0 "" 0 hf hl), (Operator "var" a2 "int" [] b '\0' i2 0 "" 0 hf2 hl2)] = (Operator "var" a "bool" [] (i >= i2) '\0' 0 0 "" 0 (HalFunction "" [] "") (HalLambda [] ""))
supEqual [(Operator "error" a "error" [] _ '\0' i 0 "" e hf hl), y] = errorIndex e
supEqual [x, (Operator "error" a "error" [] _ '\0' i 0 "" e hf hl)] = errorIndex e
supEqual k = errorIndex 24

infEqual [(Operator "var" a "int" [] _ '\0' i 0 "" 0 hf hl), (Operator "var" a2 "int" [] b '\0' i2 0 "" 0 hf2 hl2)] = (Operator "var" a "bool" [] (i <= i2) '\0' 0 0 "" 0 (HalFunction "" [] "") (HalLambda [] ""))
infEqual [(Operator "error" a "error" [] _ '\0' i 0 "" e hf hl), y] = errorIndex e
infEqual [x, (Operator "error" a "error" [] _ '\0' i 0 "" e hf hl)] = errorIndex e
infEqual k = errorIndex 25

cond x = readCond (x)

execCond [x,y] xs fns = if (readBool (executeSkip (x) fns)) then executeSkip y fns else (readCond xs fns)
execCond [] xs fns = errorIndex 18

readCond [] fns = errorIndex 18
readCond (x:xs) fns = execCond (getArguments (skipPar x)) xs fns

solveQuote (Operator "quote" a "quote" _ _ _ _ _ q _ _ _) fns = executeSkip q fns
solveQuote x fns = x

solveQuotes [] fns = []
solveQuotes (x:xs) fns = [solveQuote x fns] ++ solveQuotes xs fns

executeOp "car" array = car array
executeOp "cdr" array = cdr array
executeOp "list" array = list_fn array
executeOp "cons" array = cons array
executeOp "eq?" array = equal array
executeOp "mod" array = modulo array
executeOp "*" array = mul array
executeOp "div" array = division array
executeOp "-" array = sub array
executeOp "+" array = plus array
executeOp "<" array = inf array
executeOp ">" array = sup array
executeOp "<=" array = infEqual array
executeOp ">=" array = supEqual array
executeOp operator array = errorIndex 84

opRef line = if (isOperator (getOperator line) == False) then ("list") else (getOperator line)
opRef2 line = if (isOperator (getOperator line) == False) then ("list " ++ line) else (line)
fexec [] fns = []
fexec (x:xs) fns = [executeSkip (x) fns] ++ (fexec xs fns)

wordMine [] = []
wordMine (')':xs) = []
wordMine ('\t':xs) = []
wordMine (' ':xs) = []
wordMine (x:xs) = [x] ++ (wordMine xs)

wordskip [] = []
wordskip (')':xs) = []
wordskip (' ':xs) = xs
wordskip ('\t':xs) = xs
wordskip (x:xs) = wordskip xs

wordsMine lst = if ((wordMine lst) == []) then [] else ([wordMine lst] ++ wordsMine (skipSpaceTab (wordskip lst)))

getDefineName [x,y] = getOperator (skipPar x)
getDefineArgs [x,y] = wordsMine (skipOperator (skipPar x))
getDefineCode [x, y] = y

craftFunction (hf) = (Operator "function" [] "function" [] False '\0' 0 0 "" 0 hf (HalLambda [] ""))
craftLambda (hl) = (Operator "lambda" [] "lambda" [] False '\0' 0 0 "" 0 (HalFunction "" [] "") hl)
craftQuote (q) = (Operator "quote" [] "quote" [] False '\0' 0 0 q 0 (HalFunction "" [] "") (HalLambda [] ""))
craftQuoteArray [q:qs] = (Operator "quote" [] "quote" [] False '\0' 0 0 q 0 (HalFunction "" [] "") (HalLambda [] ""))

createLambdaInterior [x,y] = (HalLambda (wordsMine (skipPar x)) y)
createLambda line = createLambdaInterior (getArguments (line))

execLambda (Operator "lambda" [] "lambda" [] False '\0' 0 0 "" 0 (HalFunction "" [] "") hl) args fns = lambda_ hl args fns
execLambda (Operator "error" [] "error" [] False '\0' 0 0 "" e (HalFunction "" [] "") hl) args fns = errorIndex e
lambda_ (HalLambda fargs code) args fns = executeSkip (fullref fargs args code) fns

execLambdaInterior (x:xs) fns = execLambda (executeSkip x fns) xs fns
define args = craftFunction (HalFunction (getDefineName args) (getDefineArgs args) (getDefineCode args)) 

isLambda (Operator "lambda" [] "lambda" [] False '\0' 0 0 "" 0 (HalFunction "" [] "") hl) = True
isLambda x = False

resLambda (x:xs) fns = (executeSkip x fns)
runLambda (x:xs) fns = (execLambda (executeSkip x fns) xs fns)

runLambda2 line hl (x:xs) fns = if (isLambda hl) then execLambda hl xs fns else executeOp (opRef (line)) (solveQuotes (fexec (getArguments (skipOperator (opRef2 line))) fns) fns)

executeLambdaCreation line fns = if ((getOperator line) == "lambda") then (craftLambda (createLambda (skipOperator (line)))) else executeQuote line fns
executeQuote line fns = if ((getOperator line) == "quote" || (getOperator line) == "'") then (craftQuoteArray [getArguments (skipOperator (line))]) else (executeFunction line fns)
executeFunction line fns = if (isHalFunction fns (getOperator line)) then (call (getOperator line) fns (getArguments (skipOperator line)) fns) else (executeLet line fns)
executeLet line fns = if ((getOperator line) == "let") then (letMotor (getArguments (skipOperator line)) fns) else (executeDefine line fns)
executeDefine line fns = if ((getOperator line) == "define") then (define (getArguments (skipSpaceTab (skipOperator line)))) else (execute line fns)
execute line fns = if ((getOperator line) == "cond") then cond ((getArguments (skipSpaceTab (skipOperator line)))) fns else (execsp line fns)
execsp line fns = if (isOperand line == False) then (if ((isOperator (getOperator line)) == False && length (getArguments line) >= 1) then executeLambda line fns else executeOp (opRef (line)) (solveQuotes (fexec (getArguments (skipOperator (opRef2 line))) fns) fns)) else createOperand line
executeLambda line fns = runLambda2 line (resLambda ((getArguments (line))) fns) (getArguments (line)) fns
executeSkip line fns = executeLambdaCreation (skipPar (refactorQuote ' ' line)) fns

printSpace [] = []
printSpace [x] = x
printSpace (x:xs) = x ++ " " ++ (printSpace xs)

printing (Operator "var" a "int" _ _ _ i _ _ _ _ _) = show i
printing (Operator "var" a "bool" _  True _ _ _ _ _ _ _) = "#t"
printing (Operator "var" a "bool" _  False _ _ _ _ _ _ _) = "#f"
printing (Operator "var" a "list" l  False _ _ _ _ _ _ _) = "(" ++ (printList l) ++ ")"
printing (Operator "error" a "error" _  _ _ _ _ _ e _ _) = errorString e
printing (Operator "function" a "function" _ _ _ _ _ _ _ (HalFunction fname fargs code) _) = fname --"(" ++ fname ++ " " ++ (printSpace fargs) ++ ")"
printing (Operator "lambda" a "lambda" _ _ _ _ _ _ _ _ (HalLambda fargs code)) = "(lambda (" ++ printList2 fargs ++ ") " ++ code ++ ")"
printing (Operator "quote" a "quote" _ _ _ _ _ q _ _ _) = q
printList [] = ""
printList [x] = printing x
printList (x:xs) = printing x ++ " " ++ printList xs

printList2 [] = ""
printList2 [x] = x
printList2 (x:xs) = x ++ " " ++ printList2 xs
interpreter = do
                putStr "> "
                line <- getLine
                putStrLn (printing (executeSkip line []))
                interpreter

isHalFunctionOperator (Operator "function" a "function" _ _ _ _ _ _ _ (HalFunction fname fargs code) _) = True
isHalFunctionOperator _ = False

addHalFunction fns (Operator "function" a "function" _ _ _ _ _ _ _ (HalFunction fname fargs code) _) = fns ++ [(HalFunction fname fargs code)]
addHalFunction fns o = fns

isHalFunction [] (fncurrent) = False
isHalFunction ((HalFunction fname fargs code):fns) fncurrent = if (fname == fncurrent) then (True) else (isHalFunction fns fncurrent)

rfns fns resp = if (isHalFunctionOperator (resp)) then ((resp, addHalFunction fns (resp))) else (resp, fns)

rfns_exec line fns = rfns fns (executeSkip line fns)

rfns_exec2 lines (resp, fns) = exec lines fns

rfns_printing (resp, fns) = printing (resp)

rerun resp xs x fns = if (isError resp) then (exitWith (ExitFailure 84)) else (putStr "")

rfns_final [] (resp, fns) = if (isHalFunctionOperator resp) then (putStr "") else (putStrLn (printing resp))
rfns_final (x:xs) (resp, fns) = if (isHalFunctionOperator resp) then (do rfns_final xs (rfns_exec x fns)) else (do
                                                                                                                    putStrLn (printing (resp))
                                                                                                                    rerun resp xs x fns
                                                                                                                    rfns_final xs (rfns_exec x fns))
isError (Operator "error" a "error" _ _ _ _ _ _ e _ _) = True
isError x = False
exec (x:xs) fns = rfns_final xs (rfns_exec x fns) 

deleteN [] = []
deleteN ('\n':xs) = deleteN xs
deleteN (x:xs) = [x] ++ deleteN xs

readFiles [] = putStr ""
readFiles (x:xs) = do
                    k <- readFile x
                    exec (getArguments (deleteN k)) []
                    readFiles xs             
                
main :: IO ()
main = do
     y <- getArgs
     readFiles y
-- case length y of
--	  0 
-- print y
