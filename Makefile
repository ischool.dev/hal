all : make

make :
	stack build --ghc-options -O2
	cp `stack path --local-install-root`/bin/hal-exe hal
clean :
	rm -rf funhal/.stack-work
	rm -rf .stack-work
	rm -rf funhal/app/*.hs~
	rm -rf funhal/app/*.o
	rm -rf funhal/app/*.hi
	rm -rf funhal/src/*.hs~
	rm -rf funhal/src/*.o
	rm -rf funhal/src/*.hi
	rm -rf .gitignore~
	rm -rf Makefile~
	rm -rf stack.yaml~
	rm -rf funhal/stack.yaml~

fclean : clean
	rm -f ./hal

re : fclean all
